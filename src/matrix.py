import asyncio
import nats
import msgpack
from loguru import logger
import httpx
from aqua import Aqua

from config import *

client = httpx.AsyncClient()


class Bot:
    def __init__(self, aqua: Aqua):
        self.aqua = aqua

    async def on_msg(self, msg):
        data = msgpack.unpackb(msg.data)

        if data['sender'] not in SENDER_WHITELIST:
            return None

        logger.debug(data)
        message = data['body'].strip()

        try:
            async for llm_msg in self.aqua.chat(message):
                await self.send(llm_msg)
        except Exception as e:
            logger.exception(e)

    async def send(self, html):
        await self.nc.publish(f'{NATS_TOPIC_PREFIX}_action', msgpack.packb({
            'method': 'client.send_text',
            'kwargs': {
                'room_id': AQUA_ROOM_ID,
                'html': html
            }}))

    async def run(self):
        self.nc = await nats.connect(NATS_HOST)
        await self.nc.subscribe(f'{NATS_TOPIC_PREFIX}_message', cb=self.on_msg)
        await asyncio.Event().wait()


async def main():
    aqua = await Aqua.init()
    bot = Bot(aqua)
    await bot.run()


if __name__ == '__main__':
    asyncio.run(main())
