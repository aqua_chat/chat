from opensearchpy import AsyncOpenSearch
import time
from typing import Self
import httpx
from loguru import logger

from config import *

opensearch_client = AsyncOpenSearch(
    hosts=OPENSEARCH_HOST,
    http_auth=OPENSEARCH_AUTH,
    verify_certs=False,
    ssl_show_warn = False
)

beam_client = httpx.AsyncClient(base_url=BEAM_URL, auth=BEAM_AUTH, verify=True)


async def ollama(method, path, timeout=60, **kwargs):
    result = await beam_client.post('/', json={**kwargs, **{'timeout': timeout, 'method': method, 'path': path}},
                                    timeout=timeout)

    if 100 <= result.status_code < 400:
        return result.json()
    else:
        logger.error('failed to call ollama at beam')
        logger.error(f'{result=} {result.url=} {result.headers=}')
        logger.error(result.text)
    return False


class OpenSearch:
    index: str
    mappings: dict
    is_index_exists: bool

    knn_index = False

    @classmethod
    async def init(cls) -> Self:
        self = cls()
        self._is_index_exists = await self._check_index_exist()

        if not self._is_index_exists:
            print(f'{self.index} not found')
            await self._create_index()
        return self

    async def _check_index_exist(self) -> bool:
        return await opensearch_client.indices.exists(self.index)

    async def _create_index(self) -> None:
        body = {
            "settings": {
                "index": {
                    "number_of_replicas": 0,
                    "knn": self.knn_index
                }
            },
            "mappings": {
                "properties": self.mappings
            }
        }
        await opensearch_client.indices.create(self.index, body)

    async def post_doc(self, *args, **kwargs) -> str:
        result = await opensearch_client.index(self.index, body=kwargs)
        return result['_id']


class AquaOpenSearch(OpenSearch):
    index = OPENSEARCH_INDEX
    knn_index = True
    mappings = {
        "create_datetime": {
            "type": "date",
            # 1553391286000
            "format": "epoch_millis"
        },
        "input": {
            "type": "match_only_text"
        },
        "output": {
            "type": "match_only_text"
        },
        "embedding": {
            "type": "knn_vector",
            "dimension": 2048,
            "method": {
                "name": "hnsw",
                "engine": "lucene",
                "space_type": "l2",
                "parameters": {
                    "ef_construction": 128,
                    "m": 16
                }
            }
        },
        "summary": {
            "type": "match_only_text"
        }
    }

    async def search(self, embedding: list[float], k: int) -> list[dict[str, str]]:
        query = {
            "knn": {
                "embedding": {
                    "vector": embedding,
                    "k": k
                }
            }
        }
        result = await opensearch_client.search(index=OPENSEARCH_INDEX, body={
            "size": k, "query": query, "_source": {"include": ['input', 'output']}
        })

        if hits := result['hits']['hits']:
            return [hit['_source'] for hit in hits]


class MementoOpensearch(OpenSearch):
    index = 'aqua-memento'
    mappings = {
        "summary": {
            "type": "match_only_text"
        }
    }
    doc_id = 'memento'

    async def get(self) -> dict | bool:
        try:
            result = await opensearch_client.get(self.index, self.doc_id, _source=True)
            return result['_source']
        except Exception:
            return False

    async def upsert(self, doc: dict) -> None:
        await opensearch_client.update(self.index, id=self.doc_id, body={'doc': doc, 'doc_as_upsert': True})


class Memento:
    store: dict
    opensearch: MementoOpensearch

    @classmethod
    async def init(cls) -> Self:
        self = cls()
        self.opensearch = await MementoOpensearch.init()

        if not (store := await self.opensearch.get()):
            await self.opensearch.upsert({'summary': ''})
            self.store = dict()
        else:
            self.store = store

        return self

    async def get(self, key, default):
        if key in self.store:
            return self.store[key]

        await self.opensearch.upsert({key: default})
        self.store[key] = default
        return default

    async def set(self, key, value):
        await self.opensearch.upsert({key: value})
        self.store[key] = value


class Aqua:
    ring_buffer_size = 2
    summary_token_limit = 200
    model = 'mistral-openorca'

    opensearch: AquaOpenSearch
    memento: Memento
    _ring_buffer: list

    @classmethod
    async def init(cls) -> Self:
        self = cls()

        self.memento = await Memento.init()
        self._ring_buffer = await self.memento.get('ring', [])

        self.opensearch = await AquaOpenSearch.init()

        return self

    async def get_system_prompt(self, input: str) -> str:
        input_embedding = await self.gen_embedding(input)

        search_results = await self.opensearch.search(input_embedding, 2)

        memory = ''
        if search_results:
            memory = f'One of the memory:\n  Benny: {search_results[0]["input"]}\n  Aqua: {search_results[0]["output"]}'
            if len(search_results) == 2:
                memory += f'\nAnother memory: \n  Benny: {search_results[1]["input"]}\n  Aqua: {search_results[1]["output"]}'

        return f"""You are Aqua, a 25 years old female, and you were texting with a 26 years old male, Benny. 
You also remember these conversations, ignore these if they are not related to the current conversation.
{memory}

Here is the summary of the recent chat:
{await self.memento.get('summary', '')}

As Aqua, text with Benny, try to keep your response short."""

    async def get_ring_buffer(self) -> list[dict[str, str]]:
        result = []
        if len(self._ring_buffer) == 0:
            return []

        for row in self._ring_buffer:
            user_msg, llm_msg = row

            result.append({'role': 'user', 'content': user_msg})
            result.append({'role': 'assistant', 'content': llm_msg})

        return result

    async def set_ring_buffer(self, msgs: list[str, str]) -> None:
        if len(self._ring_buffer) == self.ring_buffer_size:
            self._ring_buffer.pop(0)

        self._ring_buffer.append(msgs)
        await self.memento.set('ring', self._ring_buffer)

    async def summarize(self) -> str:
        new_lines = ''
        for chat in self._ring_buffer:
            user_msg, llm_msg = chat
            new_lines += f"""Benny: {user_msg}
Aqua: {llm_msg}"""

        system_prompt = """You are Aqua, 25 years old female, and summarizing text messages with Benny, 26 years old male, as a paragraph, based on the new lines of conversation and previous summary.
Start your summary with "Summary: "
"""
        prompt = f"""Previous summary:
{await self.memento.get('summary', '')}

New lines of conversation:
{new_lines}

New summary:"""
        payload = [
            {'role': 'system', 'content': system_prompt},
            {'role': 'user', 'content': prompt}
        ]
        options = {
            'temperature': 0,
            'top_k': 40,
            'top_p': 0.7,
            'num_predict': 128
        }
        result = await ollama('post', '/api/chat', model=self.model, messages=payload, options=options, stream=False)
        await self.memento.set('summary', result['message']['content'])

        return await self.memento.get('summary', '')

    async def chat(self, input):
        messages = [
            {'role': 'system', 'content': await self.get_system_prompt(input)},
            {'role': 'user', 'content': input}
        ]

        messages[1:1] = await self.get_ring_buffer()

        options = {
            'temperature': 0.99,
            'seed': 26,
            'top_k': 46
        }

        result = await ollama('post', '/api/chat', model=self.model, messages=messages, options=options, stream=False)
        llm_msg = result['message']['content'].strip()
        await self.set_ring_buffer([input, llm_msg])

        yield llm_msg

        await self.summarize()

        embedding = await self.gen_embedding(input, llm_msg)
        await self.opensearch.post_doc(
            create_datetime=int(time.time_ns() // 1e6),
            input=input,
            output=llm_msg,
            embedding=embedding
        )

    @staticmethod
    async def gen_embedding(input, output=None) -> list[float]:
        if output:
            doc = f'Benny: {input}\nAqua: {output}'
        else:
            doc = f'Benny: {input}'

        result = await ollama('post', '/api/embeddings', model='tinyllama', prompt=doc)

        return result['embedding']

