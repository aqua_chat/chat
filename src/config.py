"""
A stub, will be overwritten by kubernetes configmap.
To update the value, copy this file to kubernetes/, then edit, and kubectl apply -k .
"""

OPENSEARCH_AUTH = ('', '')
OPENSEARCH_INDEX = ''
OPENSEARCH_HOST = ''
BEAM_AUTH = ('', '')
BEAM_URL = ''

AQUA_ROOM_ID = ''
NATS_TOPIC_PREFIX = ''
NATS_HOST = ''
SENDER_WHITELIST = ('', '')