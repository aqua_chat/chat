FROM python:3.12-alpine

RUN adduser -SDH -s /bin/sh aqua

WORKDIR /app
COPY requirements.txt .

RUN pip install -r requirements.txt

COPY src/ .

USER aqua

CMD python3 matrix.py